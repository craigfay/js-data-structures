/**
 * Linked List Module
 * 
 */

class LinkedList {
    constructor(head) {
        this.Node = class {
            constructor(data, next) {
                this.data = data;
                this.next = next;
            }
        }
        this.head = new this.Node(head);
    }
    get(index) {
        let head = this.head;
        for (i = 0; i < index; i++) {
            head = head.next;
        }
        return head.data;
    }
    push(data) {
        this.head = new this.Node(data, this.head);
    }
}

module.exports = LinkedList;
